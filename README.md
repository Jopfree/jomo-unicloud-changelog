# 用uniCloud对更新日志的增删改查

克隆项目 进入文件夹，把项目（实现用uniCloud对更新日志的增删改查）拖进HbuilderX

<img src="https://tva2.sinaimg.cn/large/006DFWgBgy1gzz1hdnqj5j30m20jiad4.jpg" alt="jomo-changlog (3)" style="zoom:50%;" />

![jomo-changlog (2)](https://tva2.sinaimg.cn/mw690/006DFWgBgy1gzz1nesrajj30wu0j8430.jpg)

![jomo-changlog (1)](https://tva4.sinaimg.cn/mw690/006DFWgBgy1gzz1p1xpxmj30tx0fndj1.jpg)


### 部署uniCloud
1、 新建/关联一个云服务空间；
2、 在云函数文件夹cloudfunctions，右键选-[上传所有云函数...]：将云函数上传到云端，可在云服务空间查看;
3、 进入云函数在文件夹changelog，右键选-[运行本地云函数]：执行创建一张changelog数据库表，可在云服务空间查看;
4、 在文件夹database，右键选-[上传所有DB-Schema]：修改数据表结构，允许增删改查权限为true;
5、 以上步骤，完成基本就可以对更新记录表进行增删改查了

## 项目UI

1、使用了[ColorUI](https://github.com/weilanwl/ColorUI)

### 问题

1、 提示：失败原因：此应用 DCloud APPID 为 __UNI__xxxxx ，您不是这个应用的项目成员。
解决：manifest.json点重新获取下AppID

### 交流
有需要可加wx：ImGeomPa或进qq群857064044交流